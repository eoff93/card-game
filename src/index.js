import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.css';
import App from './app/App';
import configureStore from './state/store/configureStore';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
