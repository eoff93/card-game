import React from 'react';
import PropTypes from 'prop-types';
import StyledButton from './Button.style';

const Button = ({ label, onClick: handleClick, primary }) => (
  <StyledButton onClick={handleClick} primary={primary}>
    {label}
  </StyledButton>
);

Button.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  primary: PropTypes.bool,
};

Button.defaultProps = {
  label: 'Click me',
  primary: false,
};

export default Button;
