import styled from 'styled-components';

const StyledButton = styled.button`
  color: #fff;
  background-color: ${props => (props.primary ? '#16AB39' : '#4C638F')};
  margin-left: 0.5em;
  margin-right: 0.5em;
  padding: 1em;
  border-radius: 6px;
  cursor: pointer;
  outline: none;
  font-weight: bold;
  border: none;
  
  &:hover {
    background-color: ${props => (props.primary ? '#008513' : '#667DA9')};
  }
`;

export default StyledButton;