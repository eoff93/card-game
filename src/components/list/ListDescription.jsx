import React from 'react';
import PropTypes from 'prop-types';
import { Description } from './ListItem.style';

const ListDescription = ({ text }) => (
  <Description>
    Score: {text}
  </Description>
);

ListDescription.propTypes = {
  text: PropTypes.number.isRequired,
};

export default ListDescription;
