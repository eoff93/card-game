import React from 'react';
import PropTypes from 'prop-types';
import ListHeader from './ListHeader';
import ListDescription from './ListDescription';
import { Content } from './ListItem.style';

const ListContent = ({ content }) => (
  <Content>
    <ListHeader text={content.header} />
    <ListDescription text={content.description} />
  </Content>
);

ListContent.propTypes = {
  content: PropTypes.shape({
    header: PropTypes.number,
    description: PropTypes.number,
  }).isRequired,
};

export default ListContent;
