import React from 'react';
import PropTypes from 'prop-types';
import { Header } from './ListItem.style';

const ListHeader = ({ text }) => (
  <Header>
    Player {text + 1}
  </Header>
);

ListHeader.propTypes = {
  text: PropTypes.number.isRequired,
};

export default ListHeader;
