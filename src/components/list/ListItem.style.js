import styled from 'styled-components';

export const Item = styled.div`
  display: inline-block;
  height: auto;
  &:not(:last-of-type) {
    margin-right: 1.5em;
  }
`;

export const Image = styled.img`
  height: 40px;
  display: inline-block;
  margin-right: 0.5em;
  position: relative;
  top: 5px;
`;

export const Header = styled.p`
  margin: 0;
  color: #E9C46A;
  font-weight: bold;
  float: left;
`;

export const Description = styled.p`
  margin: 0;
  font-weight: bold;
`;

export const Content = styled.div`
  display: inline-block;
  height: auto;
`;