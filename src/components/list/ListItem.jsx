import React from 'react';
import PropTypes from 'prop-types';
import ListContent from './ListContent';
import { Image, Item } from './ListItem.style';

const ListItem = ({ content }) => (
  <Item>
    <Image src="https://image.flaticon.com/icons/svg/149/149071.svg" alt="avatar" />
    <ListContent content={content} />
  </Item>
);

ListItem.propTypes = {
  content: PropTypes.shape({
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    description: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }).isRequired,
};

export default ListItem;
