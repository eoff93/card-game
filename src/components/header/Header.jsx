import React from 'react';
import PropTypes from 'prop-types';
import { StyledHeader, Title } from './Header.style';

const Header = ({ text }) => (
  <StyledHeader>
    <Title>{text}</Title>
  </StyledHeader>
);

Header.propTypes = {
  text: PropTypes.string.isRequired,
};


export default Header;

