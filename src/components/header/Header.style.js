import styled from 'styled-components';

export const StyledHeader = styled.div`
  background-color: #222;
  height: auto;
  padding: 20px;
  color: white;
  margin-bottom: 2em;
`;

export const Title = styled.h1`
  text-align: center;
`;