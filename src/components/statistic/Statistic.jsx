import React from 'react';
import PropTypes from 'prop-types';
import { Content, Description, Header } from './Statistic.style';

const Statistic = ({ label, value }) => (
  <Content>
    <Header>{value}</Header>
    <Description>{label}</Description>
  </Content>
);

Statistic.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default Statistic;
