import styled from 'styled-components';

export const Content = styled.div`
  display: inline-block;
  margin-bottom: 2em;
  
  &:not(:last-of-type) {
    margin-right: 2em;
  }
`;

export const Header = styled.h3`
  color: #E9C46A;
  display: block;
  font-weight: bold;
  text-transform: uppercase;
  margin: 0;
`;

export const Description = styled.p`
  display: block;
  margin: 0;
  text-transform: uppercase;
  font-weight: bold;
`;
