import React from 'react';
import PropTypes from 'prop-types';
import Button from '../button/Button';
import { SplashContainer, SplashContent, Title } from './Splash.style';

const Splash = ({ text, action, open }) => (
  <SplashContainer className={open ? 'open' : ''}>
    <SplashContent>
      <Title>{text}</Title>
      <Button onClick={action} label={'Reset'} primary />
    </SplashContent>
  </SplashContainer>
);

Splash.propTypes = {
  text: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default Splash;
