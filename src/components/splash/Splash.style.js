import styled from 'styled-components';

export const SplashContainer = styled.div`
  position: fixed;
  height: 100vh;
  width: 100vw;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.0);
  transition: background-color 0.5s, visibility 0s 0.5s;
  visibility: hidden;
  z-index: 1001;
  
  * {
    opacity: 0;
    transition: opacity 0.5s, visibility 0s 0.5s;
  }
  
  &.open {
    background-color: rgba(51, 51, 51, .8);
    visibility: visible;
    
    * {
      visibility: visible;
      opacity: 1;
    }
  }
`;

export const Title = styled.h1`
  color: #fff;
`;

export const SplashContent = styled.div`
  position: relative;
  top: 30%;
`;