import styled from 'styled-components';

export const StyledSelect = styled.select`
  color: #333;
  background-color: #fff;
  margin: 1em 0.5em;
  padding: 0.2em 3.1em;
  border-radius: 6px;
  cursor: pointer;
  outline: none;
  border: 2px solid lightgray;
  display: block;
  margin: 0 auto;
`;

export const Label = styled.label`
  font-weight: bold;
  display: inline-block;
`;

export const SelectWrapper = styled.label`
  display: inline-block;
  vertical-align: bottom;
`;