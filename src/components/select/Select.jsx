import React from 'react';
import PropTypes from 'prop-types';
import { Label, SelectWrapper, StyledSelect } from './Select.style';

const Select = ({ onChange: handleChange, options, value }) => {
  const renderOptions = options.map(option => (
    <option value={option.value} key={option.key}>
      {option.text}
    </option>
  ));

  return (
    <SelectWrapper>
      <Label htmlFor="number-of-players">Number of players</Label>
      <StyledSelect id="number-of-players" value={value} onChange={handleChange}>
        {renderOptions}
      </StyledSelect>
    </SelectWrapper>
  );
};

Select.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.number,
    text: PropTypes.string,
    value: PropTypes.number,
  })).isRequired,
  value: PropTypes.number.isRequired,
};

export default Select;
