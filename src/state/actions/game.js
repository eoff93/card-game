import * as gameActions from '../../constants/gameActionTypes';

export const pickCard = payload => ({
  type: gameActions.PICK_CARD,
  payload,
});

export const startRound = () => ({
  type: gameActions.START_ROUND,
});

export const endRound = () => ({
  type: gameActions.END_ROUND,
});

export const updateScores = payload => ({
  type: gameActions.UPDATE_SCORES,
  payload,
});

export const initScores = payload => ({
  type: gameActions.INIT_SCORES,
  payload,
});

export const changeNumberOfPlayers = payload => ({
  type: gameActions.CHANGE_NUMBER_OF_PLAYERS,
  payload,
});

export const finishGame = payload => ({
  type: gameActions.FINISH_GAME,
  payload,
});

export const resetGame = () => ({
  type: gameActions.RESET_GAME,
});

const getCardScore = (card) => {
  switch (card.value) {
    case 'ACE':
      return 1;
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '10':
      return parseInt(card.value, 10);
    case 'JACK':
      return 12;
    case 'QUEEN':
      return 13;
    case 'KING':
      return 14;
    default:
      return card.value;
  }
};

const getMaxOfArray = arr => Math.max.apply(null, arr);

const getLastIndexOfValue = (arr, value) => arr.lastIndexOf(value);

const getArraySum = arr => arr.reduce((acc, next) => acc + next);

export const determineAndUpdateScores = cards => (dispatch) => {
  const parsedCards = cards.map(getCardScore);
  const sum = getArraySum(parsedCards);
  const maxValue = getMaxOfArray(parsedCards);
  const lastIndexOfMaxValue = getLastIndexOfValue(parsedCards, maxValue);

  dispatch(updateScores({
    index: lastIndexOfMaxValue,
    value: sum,
  }));
};

export const pickCards = cards => (dispatch) => {
  dispatch(startRound());

  cards.forEach((card, idx) => {
    setTimeout(() => {
      dispatch(pickCard(card));
    }, idx * 200);
  });

  setTimeout(() => {
    dispatch(endRound());
  }, 600);
};
