import 'isomorphic-fetch';
import * as deckActionTypes from '../../constants/deckActiontTypes';

export const getDeckAsyncRequest = payload => ({
  type: deckActionTypes.GET_DECK_ASYNC_REQUEST,
  payload,
});

export const getDeckAsyncSuccess = payload => ({
  type: deckActionTypes.GET_DECK_ASYNC_SUCCESS,
  payload,
});

export const getDeckAsyncFailure = payload => ({
  type: deckActionTypes.GET_DECK_ASYNC_FAILURE,
  payload,
});

export const discardCard = payload => ({
  type: deckActionTypes.DISCARD_CARD,
  payload,
});

export const drawCardsAsyncRequest = payload => ({
  type: deckActionTypes.DRAW_CARDS_ASYNC_REQUEST,
  payload,
});

export const drawCardsAsyncSuccess = payload => ({
  type: deckActionTypes.DRAW_CARDS_ASYNC_SUCCESS,
  payload,
});

export const drawCardsAsyncFailure = payload => ({
  type: deckActionTypes.DRAW_CARDS_ASYNC_FAILURE,
  payload,
});

export const finishDrawingCards = payload => ({
  type: deckActionTypes.FINISH_DRAWING_CARDS,
  payload,
});

export const resetDeck = () => ({
  type: deckActionTypes.RESET_DECK,
});

const baseUrl = 'https://deckofcardsapi.com/api';
const buildUrl = section => baseUrl + section;

const chunkify = (arr, chunkSize) => {
  const groups = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
    groups.push(arr.slice(i, i + chunkSize));
  }
  return groups;
};

export const drawCardsAsync = (section, size) => (dispatch) => {
  dispatch(drawCardsAsyncRequest());
  return fetch(buildUrl(section), { method: 'GET' })
    .then((res) => {
      if (!res.ok) {
        throw Error(res.statusText);
      }
      return res.json();
    })
    .then((data) => {
      if (!data.cards) {
        throw Error('No cards received');
      }
      dispatch(drawCardsAsyncSuccess(chunkify(data.cards, size)));
      dispatch(finishDrawingCards());
    })
    .catch(() => {
      dispatch(drawCardsAsyncFailure());
    });
};

export const discardPickedCards = (pickedCards, cardsDrawn) => (dispatch) => {
  const indices = pickedCards.map((picked, idx) => (
    cardsDrawn[idx].findIndex(drawn => picked.value === drawn.value)
  ));

  // discard player card
  dispatch(discardCard({
    deckToRemoveFrom: 0,
    indexToRemove: indices[0],
  }));

  // discard ai cards clockwise with a delay
  for (let i = 1; i < indices.length; i += 1) {
    setTimeout(() => {
      dispatch(discardCard({
        deckToRemoveFrom: i,
        indexToRemove: indices[i],
      }));
    }, (indices.length - i) * 200);
  }
};

export const getDeckAsync = numberOfPlayers => (dispatch) => {
  dispatch(getDeckAsyncRequest());
  return fetch(buildUrl('/deck/new/shuffle/?deck_count=1'), { method: 'GET' })
    .then((res) => {
      if (!res.ok) {
        throw Error(res.statusText);
      }
      return res.json();
    })
    .then((data) => {
      if (!data.deck_id) {
        throw Error('No message received');
      }
      dispatch(getDeckAsyncSuccess(data));
      dispatch(drawCardsAsync(`/deck/${data.deck_id}/draw/?count=${10 * numberOfPlayers}`, 10));
    })
    .catch(() => {
      dispatch(getDeckAsyncFailure());
    });
};
