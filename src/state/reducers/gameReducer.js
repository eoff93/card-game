import * as gameActionTypes from '../../constants/gameActionTypes';

const initialState = {
  numberOfPlayers: 2,
  round: 1,
  status: 'Not started',
  scores: [0, 0],
  pickedCards: [],
  winner: 0,
  roundStarted: false,
};

const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case gameActionTypes.START_ROUND:
      return {
        ...state,
        pickedCards: [],
        round: state.round + 1,
        roundStarted: true,
      };
    case gameActionTypes.END_ROUND:
      return {
        ...state,
        roundStarted: false,
      };
    case gameActionTypes.PICK_CARD:
      return {
        ...state,
        pickedCards: [...state.pickedCards, action.payload],
        status: 'Started',
      };
    case gameActionTypes.INIT_SCORES:
      return {
        ...initialState,
        // fill an array with zeroes to init scores
        scores: [...Array(action.payload)].map(Number.prototype.valueOf, 0),
        round: 1,
        numberOfPlayers: action.payload,
      };
    case gameActionTypes.UPDATE_SCORES:
      return {
        ...state,
        scores: state.scores.map((score, idx) => {
          if (idx === action.payload.index) {
            return score + action.payload.value;
          }
          return score;
        }),
      };
    case gameActionTypes.CHANGE_NUMBER_OF_PLAYERS:
      return {
        ...state,
        numberOfPlayers: action.payload,
      };
    case gameActionTypes.FINISH_GAME:
      return {
        ...state,
        winner: action.payload,
      };
    case gameActionTypes.RESET_GAME:
      return {
        ...initialState,
        numberOfPlayers: state.numberOfPlayers,
      };
    default:
      return state;
  }
};

export default gameReducer;
