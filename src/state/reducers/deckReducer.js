import * as deckActionTypes from '../../constants/deckActiontTypes';

const initialState = {
  deckId: '',
  status: 'None',
  cardsDrawn: [],
  allCardsDrawn: false,
};

const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case deckActionTypes.GET_DECK_ASYNC_REQUEST:
      return {
        ...state,
        status: 'Shuffling...',
      };
    case deckActionTypes.GET_DECK_ASYNC_SUCCESS:
      return {
        ...state,
        deckId: action.payload.deck_id,
        cardsDrawn: [],
        allCardsDrawn: false,
        status: 'Deck shuffled!',
      };
    case deckActionTypes.GET_DECK_ASYNC_FAILURE:
      return {
        ...state,
        status: 'Can\'t get new deck, please check your connection',
      };
    case deckActionTypes.DRAW_CARDS:
      return {
        ...state,
        cardsDrawn: action.payload,
      };
    case deckActionTypes.DRAW_CARDS_ASYNC_REQUEST:
      return {
        ...state,
        status: 'Drawing cards',
      };
    case deckActionTypes.DRAW_CARDS_ASYNC_SUCCESS:
      return {
        ...state,
        status: 'Cards drawn!',
        cardsDrawn: [...action.payload],
      };
    case deckActionTypes.DRAW_CARDS_ASYNC_FAILURE:
      return {
        ...state,
        status: 'Can\'t draw cards, pleace check your connection',
      };
    case deckActionTypes.FINISH_DRAWING_CARDS:
      return {
        ...state,
        allCardsDrawn: true,
      };
    case deckActionTypes.DISCARD_CARD: {
      return {
        ...state,
        cardsDrawn: state.cardsDrawn.map((deck, idx) => {
          if (idx !== action.payload.deckToRemoveFrom) {
            return deck;
          }

          return [...deck.slice(0, action.payload.indexToRemove),
            ...deck.slice(action.payload.indexToRemove + 1)];
        }),
      };
    }
    case deckActionTypes.RESET_DECK: {
      return {
        ...initialState,
      };
    }
    default:
      return state;
  }
};

export default gameReducer;
