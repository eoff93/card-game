/* eslint-disable no-undef */

import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import Player from './Player';

const PlayerList = ({ scores }) => {
  const players = scores.map((score, idx) => (
    <Player key={shortid.generate()} score={score} idx={idx} />
  ));

  return (
    <div style={{ marginBottom: 20 }}>
      {players}
    </div>
  );
};

PlayerList.propTypes = {
  scores: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default PlayerList;
