/* eslint-disable no-undef */

import { connect } from 'react-redux';
import PlayerList from './PlayerList';

const mapStateToProps = state => ({
  scores: state.game.scores,
});

const PlayerListContainer = connect(
  mapStateToProps,
  null,
)(PlayerList);

export default PlayerListContainer;
