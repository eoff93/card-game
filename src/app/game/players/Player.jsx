import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '../../../components/list/ListItem';

const Player = ({ score, idx }) => {
  const content = { header: idx, description: score };
  return <ListItem content={content} />;
};

Player.propTypes = {
  score: PropTypes.number.isRequired,
  idx: PropTypes.number.isRequired,
};

export default Player;
