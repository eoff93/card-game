/* eslint-disable no-undef */

import { connect } from 'react-redux';
import PickedCards from './picked-card-list/PickedCardsList';

const mapStateToProps = state => ({
  round: state.game.round,
  pickedCards: state.game.pickedCards,
});

const PickedCardsContainer = connect(
  mapStateToProps,
  null,
)(PickedCards);

export default PickedCardsContainer;
