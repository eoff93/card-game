import styled from 'styled-components';

export const PlayingArea = styled.div`
  height: 90px; 
  margin: 0 auto;
  max-width: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 40px;
  border-radius: 10px;
`;

export const Title = styled.h3`
  margin-bottom: 0;
`;
