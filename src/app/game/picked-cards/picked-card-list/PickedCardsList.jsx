import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import PickedCard from '../picked-card/PickedCard';
import { PlayingArea, Title } from './PickedCardsList.style';

const PickedCardsList = ({ round, pickedCards }) => {
  const renderPickedCards = pickedCards.map(card => (
    <PickedCard key={shortid.generate()} card={card} />
  ));

  return (
    <div>
      <Title>
        {round > 10 ? 'Last round picks' : 'Picked cards in previous round' }
      </Title>
      <PlayingArea>
        {renderPickedCards.length === 0 &&

        <h4 style={{ color: '#fff' }}>
          None so far!
        </h4>}

        {renderPickedCards}
      </PlayingArea>
    </div>
  );
};

PickedCardsList.propTypes = {
  round: PropTypes.number.isRequired,
  pickedCards: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default PickedCardsList;
