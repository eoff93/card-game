import styled from 'styled-components';

export const Image = styled.img`
  height: 80px;
  
  &:not(:last-of-type) {
    margin-right: 6px;
  }
`;
