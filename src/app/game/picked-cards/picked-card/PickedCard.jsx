/* eslint-disable no-undef */

import React from 'react';
import PropTypes from 'prop-types';
import { Image } from './PickedCard.style';

const PickedCard = ({ card }) => (
  <Image
    src={card.image}
    alt={card.value}
    key={card.code}
  />
);

PickedCard.propTypes = {
  card: PropTypes.shape({
    image: PropTypes.string,
    code: PropTypes.string,
    value: PropTypes.string,
    suit: PropTypes.string,
  }).isRequired,
};

export default PickedCard;
