/* eslint-disable no-undef */

import { connect } from 'react-redux';
import GameCommands from './GameCommands';
import * as deckActions from '../../../state/actions/deck';
import * as gameActions from '../../../state/actions/game';

const mapStateToProps = state => ({
  numberOfPlayers: state.game.numberOfPlayers,
  winner: state.game.winner,
  allCardsDrawn: state.deck.allCardsDrawn,
});

const mapDispatchToProps = dispatch => ({
  getNewDeck: (numberOfPlayers) => {
    dispatch(deckActions.getDeckAsync(numberOfPlayers));
  },
  initScores: (numberOfPlayers) => {
    dispatch(gameActions.initScores(numberOfPlayers));
  },
  changeNumberOfPlayers: (value) => {
    dispatch(gameActions.changeNumberOfPlayers(value));
  },
  resetDeck: () => {
    dispatch(deckActions.resetDeck());
  },
  resetGame: (value) => {
    dispatch(gameActions.resetGame(value));
  },
});

const GameCommandsContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(GameCommands);

export default GameCommandsContainer;
