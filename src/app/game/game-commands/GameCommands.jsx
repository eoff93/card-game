/* eslint-disable no-undef */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../../components/button/Button';
import Select from '../../../components/select/Select';
import Splash from '../../../components/splash/Splash';

class GameCommands extends Component {
  startNewGame = () => {
    this.props.getNewDeck(this.props.numberOfPlayers);
    this.props.initScores(this.props.numberOfPlayers);
  };

  changeNumberOfPlayers = (event) => {
    this.props.changeNumberOfPlayers(parseInt(event.target.value, 10));
  };

  reset = () => {
    this.props.resetGame();
    this.props.resetDeck();
  };

  render() {
    const options = [
      { key: 1, text: '2', value: 2 },
      { key: 2, text: '3', value: 3 },
      { key: 3, text: '4', value: 4 },
    ];

    return (
      <div style={{ marginBottom: 18 }}>
        {!this.props.allCardsDrawn &&
        <Button
          onClick={this.startNewGame}
          label={'New game'}
        />
        }
        {this.props.allCardsDrawn &&
        <Button
          onClick={this.reset}
          label={'Reset'}
          primary
        />
        }
        {!this.props.allCardsDrawn &&
        <Select
          options={options}
          onChange={this.changeNumberOfPlayers}
          value={this.props.numberOfPlayers}
        />
        }
        <Splash
          text={`Player ${this.props.winner} won!`}
          action={this.reset}
          open={!!this.props.winner}
        />
      </div>
    );
  }
}

GameCommands.propTypes = {
  numberOfPlayers: PropTypes.number.isRequired,
  allCardsDrawn: PropTypes.bool.isRequired,
  winner: PropTypes.number,
  getNewDeck: PropTypes.func.isRequired,
  initScores: PropTypes.func.isRequired,
  changeNumberOfPlayers: PropTypes.func.isRequired,
  resetGame: PropTypes.func.isRequired,
  resetDeck: PropTypes.func.isRequired,
};

GameCommands.defaultProps = {
  winner: 0,
};

export default GameCommands;
