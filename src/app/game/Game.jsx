import React from 'react';
import { Container } from './Game.style';
import Header from '../../components/header/Header';
import StatsContainer from './statistics/StatsContainer';
import PlayerListContainer from './players/PlayerListContainer';
import GameCommandsContainer from './game-commands/GameCommandsContainer';
import PlayerDecksContainer from './decks/PlayerDecksContainer';

const Game = () => (
  <Container>
    <Header text={'Card Game'} />
    <StatsContainer />
    <PlayerListContainer />
    <GameCommandsContainer />
    <PlayerDecksContainer />
  </Container>
);

export default Game;
