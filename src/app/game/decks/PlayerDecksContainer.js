/* eslint-disable no-undef */

import { connect } from 'react-redux';
import PlayerDecks from './player-decks/PlayerDecks';
import * as deckActions from '../../../state/actions/deck';
import * as gameActions from '../../../state/actions/game';

const mapStateToProps = state => ({
  cardsDrawn: state.deck.cardsDrawn,
  numberOfPlayers: state.game.numberOfPlayers,
  round: state.game.round,
  scores: state.game.scores,
  roundStarted: state.game.roundStarted,
});

const mapDispatchToProps = dispatch => ({
  pickCards: (cards) => {
    dispatch(gameActions.pickCards(cards));
  },
  updateScores: (cards) => {
    dispatch(gameActions.determineAndUpdateScores(cards));
  },
  discardPickedCards: (pickedCards, drawnCards) => {
    dispatch(deckActions.discardPickedCards(pickedCards, drawnCards));
  },
  finishGame: (playerWithHighestScore) => {
    dispatch(gameActions.finishGame(playerWithHighestScore));
  },
});

const PlayerDecksContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PlayerDecks);

export default PlayerDecksContainer;
