import styled from 'styled-components';

export const PCCardWrapper = styled.div`
  display: inline-block;
  cursor: pointer;
  transition: transform 0.2s ease-in-out;
  
  &:not(:first-of-type) {
    margin-left: -32px;  
  }
  
  &:hover {
    transform: ${props => props.roundStarted ? 'none' : 'translate(0, -10px)'};
  }
`;

export const AICardWrapper = styled.div`
  display: inline-block;
  
  &:not(:first-of-type) {
    margin-left: -50px;
  }
`;

export const DeckWrapper = styled.div`
  display: inline-block;
`;
