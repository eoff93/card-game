import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import Card from '../card/Card';
import {
  AICardWrapper,
  PCCardWrapper,
  DeckWrapper,
} from './Deck.style';

const Deck = ({ deck, onCardClick, roundStarted }) => {
  const renderCards = deck.map((card) => {
    if (onCardClick) {
      return (
        <PCCardWrapper
          key={shortid.generate()}
          onClick={() => onCardClick(card)}
          roundStarted={roundStarted}
        >
          <Card
            card={card}
            isAICard={false}
          />
        </PCCardWrapper>
      );
    }
    return (
      <AICardWrapper key={shortid.generate()}>
        <Card card={card} isAICard />
      </AICardWrapper>
    );
  });

  return (
    <DeckWrapper>
      {renderCards}
    </DeckWrapper>
  );
};

Deck.propTypes = {
  deck: PropTypes.arrayOf(PropTypes.object).isRequired,
  onCardClick: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.bool,
  ]).isRequired,
  roundStarted: PropTypes.bool,
};

Deck.defaultProps = {
  roundStarted: false,
};

export default Deck;
