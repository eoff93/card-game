import styled from 'styled-components';

export const LeftPlayer = styled.div`
  position: absolute;
  transform: rotate(90deg);
  right: 240px;
  display: inline-block;
  bottom: 120px;
  width: 140px;
`;

export const RightPlayer = styled.div`
  display: inline-block;
  position: absolute;
  transform: rotate(-90deg);
  left: 240px;
  bottom: 120px;
  width: 140px;
`;
