/* eslint-disable no-undef */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import Deck from '../deck/Deck';
import PickedCardsContainer from '../../picked-cards/PickedCardsContainer';
import { LeftPlayer, RightPlayer } from './PlayerDecks.style';

class PlayerDecks extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.round > 10) {
      const maxScore = Math.max.apply(null, this.props.scores);
      const playerWithHighestScore = this.props.scores.findIndex(value => value === maxScore) + 1;

      this.props.finishGame(playerWithHighestScore);
    }
  }

  pickCards = (PCCard) => {
    if (this.props.roundStarted) {
      return;
    }

    const cardsDrawn = this.props.cardsDrawn;
    let pickedCards = [PCCard];

    // pick random cards for AI players
    for (let i = 1; i < this.props.numberOfPlayers; i += 1) {
      const randomCard = cardsDrawn[i][Math.floor(Math.random() * cardsDrawn[i].length)];
      pickedCards = pickedCards.concat(randomCard);
    }
    this.props.pickCards(pickedCards);
    this.props.updateScores(pickedCards);
    this.props.discardPickedCards(pickedCards, cardsDrawn);
  };


  render() {
    const playerDeck = this.props.cardsDrawn.length ? this.props.cardsDrawn[0] : [];
    const aiDecks = this.props.cardsDrawn.length ? this.props.cardsDrawn.slice(1) : [];

    const renderPlayerDeck = (
      <Deck
        key={shortid.generate()}
        deck={playerDeck}
        onCardClick={this.pickCards}
        roundStarted={this.props.roundStarted}
      />
    );

    const renderAIDecks = aiDecks.map(deck => (
      <Deck
        key={shortid.generate()}
        deck={deck}
        onCardClick={false}
      />
    ));

    return (
      <div>
        <div style={{ marginBottom: 16 }}>
          {renderPlayerDeck}
        </div>
        <PickedCardsContainer />

        {
          this.props.numberOfPlayers === 2 ?
            renderAIDecks :
            this.props.numberOfPlayers === 3 ?
              <div style={{ display: 'inline-block', position: 'relative', width: '140px', height: '85px' }}>
                <LeftPlayer>{renderAIDecks[0]}</LeftPlayer>
                <RightPlayer>{renderAIDecks[1]}</RightPlayer>
              </div> :
              <div style={{ display: 'inline-block', position: 'relative', width: '140px', height: '85px' }}>
                <LeftPlayer>{renderAIDecks[0]}</LeftPlayer>
                {renderAIDecks[1]}
                <RightPlayer>{renderAIDecks[2]}</RightPlayer>
              </div>
        }
      </div>
    );
  }
}

PlayerDecks.propTypes = {
  round: PropTypes.number.isRequired,
  roundStarted: PropTypes.bool.isRequired,
  scores: PropTypes.arrayOf(PropTypes.number),
  cardsDrawn: PropTypes.arrayOf(PropTypes.array),
  numberOfPlayers: PropTypes.number.isRequired,
  pickCards: PropTypes.func.isRequired,
  updateScores: PropTypes.func.isRequired,
  discardPickedCards: PropTypes.func.isRequired,
  finishGame: PropTypes.func.isRequired,
};

PlayerDecks.defaultProps = {
  cardsDrawn: [],
  scores: [],
};

export default PlayerDecks;
