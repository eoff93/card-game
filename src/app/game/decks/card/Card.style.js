import styled from 'styled-components';

export const Image = styled.img`
  height: ${props => props.isAICard ? '80px' : '100px'};
`;