import React from 'react';
import PropTypes from 'prop-types';
import backOfCard from '../../../../assets/img/card-back-blue.jpg';
import { Image } from './Card.style';

const Card = ({ card, isAICard }) => {
  const imageSrc = isAICard ? backOfCard : card.image;

  return (
    <Image
      isAICard={isAICard}
      src={imageSrc}
      alt={card.value}
      key={card.code}
    />
  );
};

Card.propTypes = {
  card: PropTypes.shape({
    image: PropTypes.string,
    value: PropTypes.string,
    suit: PropTypes.string,
    code: PropTypes.string,
  }).isRequired,
  isAICard: PropTypes.bool.isRequired,
};

export default Card;
