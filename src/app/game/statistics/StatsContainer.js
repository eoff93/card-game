/* eslint-disable no-undef */

import { connect } from 'react-redux';
import StatList from './StatList';

const mapStateToProps = state => ({
  numberOfPlayers: state.game.numberOfPlayers,
  round: state.game.round,
  deckStatus: state.deck.status,
  gameStatus: state.game.status,
});

const StatsContainer = connect(
  mapStateToProps,
  null,
)(StatList);

export default StatsContainer;
