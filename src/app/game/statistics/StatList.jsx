/* eslint-disable no-undef */

import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import Stat from './Stat';

const StatList = ({ numberOfPlayers, round, deckStatus, gameStatus }) => {
  const items = [
    { label: 'Number of players', value: numberOfPlayers },
    { label: 'Round', value: round > 10 ? 10 : round },
    { label: 'Deck status', value: deckStatus },
    { label: 'Game status', value: round > 10 ? 'Finished' : gameStatus },
  ];

  const renderStats = items.map(item => (
    <Stat key={shortid.generate()} label={item.label} value={item.value} />
  ));

  return (
    <div>
      {renderStats}
    </div>
  );
};

StatList.propTypes = {
  numberOfPlayers: PropTypes.number.isRequired,
  round: PropTypes.number.isRequired,
  deckStatus: PropTypes.string.isRequired,
  gameStatus: PropTypes.string.isRequired,
};

export default StatList;
