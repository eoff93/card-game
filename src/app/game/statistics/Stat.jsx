import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import Statistic from '../../../components/statistic/Statistic';

const Stat = ({ label, value }) => (
  <Statistic key={shortid.generate()} label={label} value={value} />
);

Stat.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

export default Stat;
