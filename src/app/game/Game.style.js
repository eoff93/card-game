import styled from 'styled-components';
import bgImage from '../../assets/img/bg.jpg';

export const Container = styled.div`
  text-align: center;
  background: url(${bgImage}) no-repeat center center;
  background-size: cover;
  min-height: 100vh;
  color: #fff;
`;
