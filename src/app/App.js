import React from 'react';
import Game from './game/Game';

const App = () => (
  <Game />
);

export default App;
