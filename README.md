# Card Game

## Synopsis

A 2-4 player card game built with ReactJS, Redux and styled-components

## Installation

`git clone https://eoff93@bitbucket.org/eoff93/card-game.git`

`cd` to cloned directory

`npm install` to get all dependencies

Run `npm start` to initiate http server

Alternatively run `yarn` => `yarn start`

## License

[MIT](https://opensource.org/licenses/MIT)
